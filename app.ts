import express from 'express'
import dotenv from 'dotenv'
import bodyParser from 'body-parser';
import { UserService } from '$services/users';
import { UserController } from '$routes/user';
import { UserRepository as UserInmemRepo } from '$services/users/repositories/inmem';
import { UserRepository as UserMySqlRepo } from '$services/users/repositories/mysql';
import 'sqlize/index'

dotenv.config();

const port = process.env.APP_PORT
const app = express()

const userInmemRepo = new UserMySqlRepo()

const userService = new UserService(userInmemRepo)
const userController = new UserController(userService, express.Router())

app.use(bodyParser.json())
app.use("/users", userController.registerRoutes())

app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});