import { Router } from "express";
import { UserService } from "$services/users";

export class UserController {
    private router: Router
    private userService: UserService

    /**
     *
     */
    constructor(userService: UserService, router: Router) {
        this.userService = userService;
        this.router = router
    }

    private handleGetAll() {
        this.router.get("/", async (req, res, next) => {
            try {
                const users = await this.userService.getAll()
                res.status(200)
                res.send(users)
            } catch (error) {
                console.log(error)
                res.status(400)
                res.send(error)
            }
        })
    }

    registerRoutes() {
        this.handleGetAll()
        return this.router
    }
}