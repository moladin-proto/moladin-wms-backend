import { User } from "$models/user";

/**
 * Specifies the contract for working with user storage either it's an inmemory storage, mysql database etc
 */
export interface UserRepository {
    findAll: () => Promise<User[]>
}

/**
 * The get all users method signature
 */
type getAllUsers = () => Promise<User[]>

/**
 * Provides the business functionalities of User
 */
export class UserService {
    private _repository: UserRepository

    constructor(userRepository: UserRepository) {
        this._repository = userRepository
    }

    getAll: getAllUsers = async (): Promise<User[]> => {
        const users = await this._repository.findAll()
        return users
    }
}