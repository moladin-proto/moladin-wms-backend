
import { User } from '$models/user';
import { UserRepository as Repository } from '$services/users'
import { UserModel } from 'sqlize/user';

export class UserRepository implements Repository {
    findAll: () => Promise<User[]> = async () => {
        const users = await UserModel.findAll()
        return Promise.resolve(users)
    }
}