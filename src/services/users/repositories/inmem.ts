import { User } from '$models/user'
import { UserRepository as Repository } from '$services/users'

export class UserRepository implements Repository {

    private users: User[] = []

    constructor(initialData: User[] = [
        {
            id: "1",
            email: "supendi@email.com",
            password: "p1"
        },
        {
            id: "2",
            email: "irpan@email.com",
            password: "p1"
        },
        {
            id: "3",
            email: "anjar@email.com",
            password: "p1"
        }
    ]) {
        this.users = initialData
    }

    private getNewId() {
        const newId = (this.users.length + 1).toString()
        return newId
    }

    findAll: () => Promise<User[]> = () => {
        return Promise.resolve(this.users)
    }

    add: (user: User) => Promise<User> = (user: User) => {
        if (!!user) {
            user.id = this.getNewId()
            this.users.push(user)
            return Promise.resolve(user)
        }
        return Promise.reject("Failed to add user")
    }
}