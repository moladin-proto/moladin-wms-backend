import { Sequelize } from "sequelize-typescript";
import { UserModel } from "./user";

/**
 * init sequelize
 */
new Sequelize({
    dialect: 'mysql',
    username: 'root',
    password: 'Irpan123!',
    host: 'localhost',
    port: 3306,
    database: "moladin-wms",
    models: [UserModel],
    logging: true
})