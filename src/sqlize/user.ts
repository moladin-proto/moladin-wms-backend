import { User } from "$models/user";
import { Model, Table, Column, DataType } from "sequelize-typescript";

/**
 * Represents the user table
 */
@Table({
    tableName: 'user',
    timestamps: false
})
export class UserModel extends Model<User> implements User {
    @Column({
        field: "id",
        type: DataType.INTEGER,
        primaryKey: true,
    })
    id: string | undefined

    @Column({
        field: "email",
        type: DataType.STRING,
    })
    email: string | undefined;

    @Column({
        field: "password",
        type: DataType.STRING,
    })
    password: string | undefined;
}