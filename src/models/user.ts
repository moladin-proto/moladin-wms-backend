/**
 * Represents the model of User entity
 */
export interface User {
    id?: string
    email?: string
    password?: string
}